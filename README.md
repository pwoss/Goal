# **We moved everything to our own Gitea instance -> [git.pwoss.xyz](https://git.pwoss.xyz/PwOSS/Goal)**

# Goal
### Privacy
The main goal is to reach more privacy for everyone. To make it easier to set up a server at home. Showing a way to install a Linux system on a PC and how to use an Android phone without Google and other important privacy needs.

&nbsp;

### Minimum Goal
- Server
  - script for easy installation
- Linux (Desktop/Laptop)
  - script to get everything connected to the server
- Android
  - script or a guide to get everything connected to the server and Linux (Desktop/Laptop) and how to get rid of Google etc.

&nbsp;

### Perfect Goal
- Server
  - own distribution
- Linux (Desktop/Laptop)
  - own distribution
- Android
  - own distribution (Custom-ROM)

&nbsp;

## Server

### Arch 64-bit & Arch ARM
We want to stay with Arch as a server to keep it as simple as possible. If major updates or downgrades are necessary let's provide a 'quick' script or news with the important commands to keep the server running.

&nbsp;

### Debian
Maybe one day.

&nbsp;

## It's best to do it yourself!
We want to show you that can be easy even for non-professionals. We want to give you the possibility to set up your private _little_ services at home. Connected with all your devices without the dependence of other companies.<br>

_No dependence. No tracking. No profiling. No collection of private data._

&nbsp;

## Info
More information about __PwOSS - Privacy with Open Source Software__ at https://pwoss.xyz/.

&nbsp;

## License
<a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license"><img style="border-width: 0;" src="https://pwoss.xyz/wp-content/uploads/2018/07/licensebutton.png" alt="Creative Commons License" /></a>
